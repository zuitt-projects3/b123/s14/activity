// Writing comments in JavaScript:
// There are two ways of writing comments in JS:
// Single line comments - ctrl + /

console.log("Hello World");

// Consoles are part of our browsers which will allows us to see/log
// messages, data or information from our programming language--JavaScript


console.log("Prince Joedymar Barro");
/*
  Statements are instructions, expressions we add to our programming
  language which will then be communicated to or computers

  Statements in JavaScript commonly ends in semicolon. However , javascript has
   an implemented way of automatically adding semicolons at the end of our statements.
   Which, therefore, mean that, unlike other languages, JS does not require
   semicolons.
*/

/*
  Syntax in programming is a set of rules that describes how statements are properly
  made/constructed.

  Lines/blocks of code must follow a certain set of rules for it tow work.
*/

/*
  Variables - are containers of data. It gives a name to describe a piece of data.
    also allows us to use or refer to data multiple times.
*/

let num = 10;
console.log(6);
console.log(num);

let name1 = "Jeffrey"
console.log("John")
console.log(name1)

/*
  Creating Variables:

  Declaration - which actually allows us to create the variable.
  Initialization - which allows to add an initial value to a variable.
*/
let myVariable;
myVariable = "New Initialized Value";
console.log(myVariable);


myVariable3 = "Another sample";
console.log(myVariable3);

// Let vs Const

/*
  With the use of let, we can create variables that can be declared, initialized and
  re-assigned.
*/

best = "naruto";
console.log(best);

best = "one piece";
console.log(best);

/*
  Const - const variiable are variables with constant data. Therefore we should not
  re-declare, re-assign or even declare a const variable without initialization
*/


const pi = 3.1416;
console.log(pi);


const goat = "MJ"
console.log(goat);
// goat = "LBJ";
// console.log(goat);

/*
  Const variables are used for data that we expect or do not
*/










/*
  Forced coercion - when one data's type is forced to change to complete an operation.
*/
// string + num = concatentation
let numString1 = "5"
let numString2 = "6"
let num1 = 5;
let num2 = 6;
let num3 = 5.5;
let num4 = .5;
console.log(num3+numString1);
console.log(num3+num4);

// parseInt() - this can change a numeric string into a proper number.
console.log(num3+parseInt(numString1));

let sum2 = num1 + parseInt(numString1);
console.log(sum2);

// -------------------------------Mathematical Operation-----------------------

// Subtraction
console.log(num1-num3);
console.log(num3-num4);

console.log(numString1-num2);

// In Subtraction strings won't concatenate, and instead will be forced to change it's data DOCTYPE

let sample2 = "prince";
console.log(sample2-numString2);

// Multiplication
console.log(num1*num2);
console.log(numString1*num1);
console.log(numString1*numString2);

// Multiplication is same like subtraction, strings also won't concatenate.

let product1 =num1*num2;
let product2 =numString1*num1;
let product3 =numString1*numString2;

// Division
console.log(product1/num2);
console.log(product2/5);
console.log(numString2/numString1);

// Division and Multiplication by 0

console.log(product2*0);
console.log(product3/0);

// Modulo - remainder of a division operator
console.log(product2%num2);
console.log(product3%product2);
console.log(num1%num2);
console.log(num1%num1);



// -----------------------------Boolean----------------------------------------

  /*
    Boolean is usually used for logic operations or for if-else conditions. it's
    usually a yes or no question.
  */

  let isAdmin = true;
  let isMarried = false;
  let isMVP = true;
  console.log("Is she married? " + isMarried);
  console.log("Is he the MVP? " + isMVP);
  console.log(`Is he the current admin? ` + isAdmin);

// -------------------------------Arrays---------------------------------------

/*
  Arrays are a special kind of data type used to store multiple values.
  Arrays can actually store data with different types BUT, as the best practice,
  arrays are used to contain multiple values of the SAME data type.

  Values in an array is seperated by a commas, and is created by an Array Literal = [].
*/

let array1 = ["Goku","Piccolo","Gohan","Vegeta"];
console.log(array1);

// --------------------------------Objects--------------------------------------

/*
  Objects are another special kind of data types used to mimic real world Objects.
  Used to create complex data that contain pieces of information that are relevant
  to each other. Objects are created with Object literals {}.

  Each data are paired with a key, Each field is called a property, Each field is
  sperated by a comma.
*/

let hero1 = {
  heroname: "Caped Baldy",
  isActive: true,
  salary: 500,
  realname: "Saitama",
  class: "B-rank # 3",
};
console.log(hero1.class);

// Mini Activity----------

let bandnames = ["Chito","Rico Blanco","Bamboo","Lisa","Yojiro Noda"]
console.log(bandnames);

let person = {
  firstname: "Oda",
  lastname: "Eiichiro",
  isDeveloper: true,
  hasportfolio: true,
  age: 46,
};
console.log(person.age);

// ------------------------------Null and Undefined-----------------------------
/*
  Null - is explicit a absence of data. This is done to project that a variable
  contains nothing over undefined as undefined merely means ther is no data in the
  variable BECAUSE the variable has not been assigned an initial value.
*/

let sampleNull = null;
console.log(sampleNull);
/*
  Undefined - is a representation that a variable has been declared but it was
  not assigned an initial value.
*/

let sampleUndefined;
console.log(sampleUndefined);

let person2 = {
  name: "peter"
}
console.log(person2.age);

// -----------------------------------Functions---------------------------------

/*
  Functions - are lines/blocks of codes that tell our device /application to
  perform a certain task when called/invoked.

            - are reusable pices of code with instructions which are used over
              and over again.

        Syntax: function functionName(){};
*/

function printName(){
  console.log("My name is Prince");
};
printName();

function showSum(){
  console.log(6+6);
};
showSum();

// Parameters and Arguments
// (name) is called a parameter.
// A parameter acts a named variable /container that exists only inside of the function.

function printName(name){
  console.log(`My name is ${name}`);
};

printName("Prince");

function displayNum(number){
  console.log(number);
};

displayNum(5000);

// Mini Activity---------

function sampleMessage(message){
  console.log(message);
};

sampleMessage("JavaScript is Fun");

// Multiple Parameters and Arguments
/*
  A function can not only receive a single argument but it can also receive multiple
  arguments so long as it matches the number of parameters in the function.
*/

function displayFullName(firstname,lastname,age){
  console.log(`${firstname} ${lastname} is ${age}`);
};
displayFullName("Prince","Barro",24);

// Return keyword

/*
  Return keyword - is used so that a function may return a value. It also stops
  the process of the function any other instruction after keyword will not be
  processed.
*/

function createFullName(firstname,middleName,lastName){
  return `${firstname} ${middleName} ${lastName}`
  console.log("I will no longer run because the function's value/result has been returned.")
};


let fullName1 = createFullName("Prince","Joedymar","Barro");
let fullName2 = displayFullName("James","Luck","Barro");

console.log(fullName1);
console.log(fullName2);

let fullname3 = createFullName("Luffy","Ace","Sabo");
console.log(fullname3);
